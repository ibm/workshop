---
title: Promocodes for IBM Cloud Account
subtitle: How to use promocodes for the IBM Cloud
comments: false
---

# Use a Promocode

**NOTE:** This is only if something has gone wrong.

Click on the upper right icon that looks like a "person", and click on
the **Profile** link.

![edit profile](../images/profile.png)

From the profile page click on the [Billing](https://console.bluemix.net/account/billing)

![billing screen](../images/billing.png)

You will be getting a Promo Code from the Google Drive link specified
in the workshop. Take one from there. Add it with the Add Promo Code
Screen.

![promo code](../images/promocode.png)
