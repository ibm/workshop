---
title: Install Developer Tools if you are doing this locally
subtitle: How to install the tools locally
comments: false
---

## What you will install

* curl - for downloading tools
* git - for downloading the git repo
* ibm cloud cli - for interacting with ibm cloud
* kubectl - for interacting with kubernetes

## Install curl / git

**On Ubuntu Linux:**

```bash
sudo apt install curl git
```

**On Mac:**

curl comes with the system

```bash
brew install git
```

#### Install IBM Cloud Cli

**On Linux:**

```bash
curl -fsSL https://clis.ng.bluemix.net/install/linux | sh
```

**On Mac:**

```bash
curl -fsSL https://clis.ng.bluemix.net/install/osx | sh
```

You then need to install the Container plugins for IBM Cloud

```bash
ibmcloud plugin install -r "IBM Cloud" container-service
ibmcloud plugin install -r "IBM Cloud" container-registry
```

#### Install Kubectl

**On Linux:**

```bash
curl --progress-bar -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
sudo mv kubectl /usr/local/bin
sudo chmod +x /usr/local/bin/kubectl
```

**On Mac:**

```
curl --progress-bar -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/darwin/amd64/kubectl
sudo mv kubectl /usr/local/bin
sudo chmod +x /usr/local/bin/kubectl
```
